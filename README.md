# Installation de Python
- Télécharger l'installeur Python 3.10 : [version 32 bits](https://www.python.org/ftp/python/3.10.0/python-3.10.0.exe) / [version 64 bits](https://www.python.org/ftp/python/3.10.0/python-3.10.0-amd64.exe)
- Lancer l'installeur, bien cocher la case "Add Python 3.10 to PATH" 

![Image de l'installeur - Etpae 0](docs/images/py_install_step0.png)
- A la dernière étape, bien cocher "Disable MAX_LENGTH" limitation on this computer

# Installation d'OpenPyXL
- Ouvrir Powershell (Windows +R => powershell => entrée)
- Lancer la commande

      pip install openpyxl
      pip install pillow

# Installation de VSCode
- [Installer VS Code](https://code.visualstudio.com/docs/?dv=win)
- Dans extensions, trouver et installer l'extension "Python"


# Lancement des examples
- Dans VS Code, ouvrir les fichiers XX_nom_programme.py
- Lancer l'éxécution en deubg avec F5
